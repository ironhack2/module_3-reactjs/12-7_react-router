import IphoneContent from "../components/content/IphoneContent";

function IphonePage() {
    return (
        <>
            <IphoneContent />
        </>
    )
}

export default IphonePage;