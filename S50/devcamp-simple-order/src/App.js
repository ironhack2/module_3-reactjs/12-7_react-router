import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Routes, Route } from "react-router-dom";

import ProductAllPage from './pages/ProductAllPage';
import IphonePage from './pages/IphonePage';
import SamsungPage from './pages/SamsungPage';
import NokiaPage from './pages/NokiaPage';

function App() {

  return (
    <>
      <Routes>
        <Route exact path="/" element={<ProductAllPage />}></Route>

        <Route path="/iphone/:param" element={<IphonePage />}></Route>
        <Route path="/" element={<IphonePage />}></Route>

        <Route path="/samsung/:param" element={<SamsungPage />}></Route>
        <Route path="/nokia/:param" element={<NokiaPage />}></Route>
      </Routes>
    </>
  );
}

export default App;
