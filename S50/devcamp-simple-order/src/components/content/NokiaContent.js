import { Container, Grid, Typography, Button } from '@mui/material';
import { Link } from "react-router-dom";
import imgNokia from '../../assets/images/nokia.jpg';


function NokiaContent() {
    return (
        <Container>
            <Grid container mt={5}>
                <Grid item xs={12}>
                    <Button variant="contained">
                        <Link to="/" style={{ color: "white", textDecoration: "none" }}>Come Back</Link>
                    </Button>
                </Grid>
            </Grid>

            <Grid container style={{ marginTop: "120px" }}>
                <Grid item xs={6}>
                    <img src={imgNokia} width="90%"></img>
                </Grid>

                <Grid item xs={6}>
                    <Typography variant="h5">
                        Điện thoại Nokia
                    </Typography>
                    <Typography variant="body1" mt={5}>
                        Nokia 105 4G - Chiếc điện thoại phổ thông nổi bật với thiết kế sang trọng, khả năng nghe gọi bền bỉ trong thời gian dài, hỗ trợ công nghệ 4G cùng nhiều tiện ích giải trí hấp dẫn khác đến từ nhà Nokia.
                        Ngoại hình pha lẫn chút hiện đại
                        Toàn thân của máy được hoàn thiện từ nhựa với lớp vỏ sáng bóng nhìn khá đẹp mắt, tạo sự sang trọng và tinh tế cho máy. Một trong những điểm cộng của thiết bị là độ bền cao hạn chế được tình trạng nứt vỡ khi vô tình va chạm hoặc làm rơi.

                        Ưu điểm tiếp theo của chiếc điện thoại này là mang cho mình tổng thể gọn nhẹ tiện lợi cho người dùng trong việc cất giữ, cầm nắm và di chuyển dễ dàng, mang đến cảm giác thoải mái khi sử dụng.

                        Máy có 3 lựa chọn màu rực rỡ đi kèm là đỏ, xanh và đen đem đến vẻ ngoài của điện thoại vô cùng thời trang mang đậm chất cá tính và năng động.
                        Kết nối 4G đột phá
                        Nokia 105 4G hoạt động trên con chip Unisoc T107 và RAM 128 MB cũng như bộ nhớ trong 48 MB, đáp ứng ổn các cho các tác vụ nghe gọi, sử dụng cơ bản.
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    )
}

export default NokiaContent;