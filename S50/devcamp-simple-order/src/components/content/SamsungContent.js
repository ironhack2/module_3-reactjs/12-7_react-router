import { Container, Grid, Typography, Button } from '@mui/material';
import { Link } from "react-router-dom";
import imgSamsung from '../../assets/images/samsung.jpg';


function SamsungContent() {
    return (
        <Container>
            <Grid container mt={5}>
                <Grid item xs={12}>
                    <Button variant="contained">
                        <Link to="/" style={{ color: "white", textDecoration: "none" }}>Come Back</Link>
                    </Button>
                </Grid>
            </Grid>

            <Grid container style={{ marginTop: "120px" }}>
                <Grid item xs={6}>
                    <img src={imgSamsung} width="90%"></img>
                </Grid>

                <Grid item xs={6}>
                    <Typography variant="h5">
                        Điện thoại Samsung
                    </Typography>
                    <Typography variant="body1" mt={5}>
                        Galaxy S22 Ultra 5G chiếc smartphone cao cấp nhất trong bộ 3 Galaxy S22 series mà Samsung đã cho ra mắt. Tích hợp bút S Pen hoàn hảo trong thân máy, trang bị vi xử lý mạnh mẽ cho các tác vụ sử dụng vô cùng mượt mà và nổi bật hơn với cụm camera không viền độc đáo mang đậm dấu ấn riêng.
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    )
}

export default SamsungContent;