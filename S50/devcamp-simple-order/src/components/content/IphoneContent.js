import { Container, Grid, Typography, Button } from '@mui/material';
import { Link } from "react-router-dom";
import imgIphone from '../../assets/images/iphone13.jpg';


function IphoneContent() {
    return (
        <Container>
            <Grid container mt={3}>
                <Grid item xs={12}>
                    <Button variant="contained">
                        <Link to="/" style={{ color: "white", textDecoration: "none" }}>Come Back</Link>
                    </Button>
                </Grid>
            </Grid>

            <Grid container style={{ marginTop: "120px" }}>
                <Grid item xs={6}>
                    <img src={imgIphone} width="90%"></img>
                </Grid>

                <Grid item xs={6}>
                    <Typography variant="h5">
                        Điện thoại iPhone 13
                    </Typography>
                    <Typography variant="body1" mt={5}>
                        Điện thoại iPhone 13 Pro Max 128 GB - siêu phẩm được mong chờ nhất ở nửa cuối năm 2021 đến từ Apple. Máy có thiết kế không mấy đột phá khi so với người tiền nhiệm, bên trong đây vẫn là một sản phẩm có màn hình siêu đẹp, tần số quét được nâng cấp lên 120 Hz mượt mà, cảm biến camera có kích thước lớn hơn, cùng hiệu năng mạnh mẽ với sức mạnh đến từ Apple A15 Bionic, sẵn sàng cùng bạn chinh phục mọi thử thách.
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    )
}

export default IphoneContent;