import RenderProduct from "./RenderProducts";
import { Container, Grid, Typography } from "@mui/material";
import { useState } from "react";


const gProducts = [
    {
        img: "//cdn.tgdd.vn/Products/Images/42/230529/iphone-13-pro-xanh-xa-1.jpg",
        name: "Iphone 13Pro Max",
        price: 900,
        route: "/iphone/13"
    },
    {
        img: "//cdn.tgdd.vn/Products/Images/42/271697/samsung-galaxy-s22-ultra-xanh-reu-1.jpg",
        name: "SamSung S22 Ultra",
        price: 800,
        route: "/samsung/20"
    },
    {
        img: "//cdn.tgdd.vn/Products/Images/42/240194/nokia-105-4g-1-1-org.jpg",
        name: "Nokia 110i",
        price: 650,
        route: "/nokia/11"
    }
]

function ProductAll() {
    const [total, setTotal] = useState(0);

    const addTotal = (value) => {
        setTotal(total + value);
    }

    return (
        <Container style={{ marginTop: "120px" }}>
            <Grid container>
                {
                    gProducts.map((product, index) => {
                        return (
                            <Grid item key={index} xs={4}>
                                <RenderProduct imgProp={product.img} nameProp={product.name} priceProp={product.price} detailLProp={product.detailL} routeProp={product.route} addTotalProp={addTotal} />
                            </Grid>
                        )
                    })
                }
            </Grid>

            <Typography variant="h6" mt={5}>
                Total: <span style={{ color: "red" }}>{total}$</span>
            </Typography>
        </Container>
    );
}

export default ProductAll;