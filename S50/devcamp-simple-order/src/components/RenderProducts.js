import { Container, Grid, Card, CardMedia, CardContent, Typography, CardActions, Button } from "@mui/material";
import { useState } from "react";
import {Link} from "react-router-dom";



function ProductAll({ imgProp, nameProp, priceProp, addTotalProp, detailLProp, routeProp }) {

    const [quanlity, setQuanlity] = useState(0);


    const onClickAddProduct = () => {
        addTotalProp(priceProp);

        setQuanlity(quanlity + 1);

        console.log("Tên sản phẩm:", nameProp);
        console.log("Giá sản phẩm:", priceProp);
        console.log("Số lượng sản phẩm:", quanlity + 1);
        console.log("");
    }


    return (
        <Container>
            <Card>
                <CardMedia
                    component="img"
                    height="auto"
                    image={imgProp}
                    alt="green iguana"
                />
                <CardContent>
                    <Typography variant="h6" component="div">
                        <b>{nameProp}</b>
                    </Typography>
                    <Typography variant="body1" mt={3}>
                        <b>Price:</b> {priceProp}$
                    </Typography>
                    <Typography variant="body1" mt={1}>
                        <b>Quanlity:</b> {quanlity}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Grid container mt={1}>
                        <Grid item xs={6} style={{ textAlign: 'left' }}>
                            <Button variant="contained" size="large" onClick={onClickAddProduct}>Buy</Button>
                        </Grid>
                        <Grid item xs={6} style={{ textAlign: 'right' }} sx={{ mt: 2 }}>
                            <Button variant="contained" color="success" size="small">
                                <Link to={routeProp} style={{textDecoration:"none", color:"white"}}>Detail</Link>
                            </Button>
                        </Grid>
                    </Grid>
                </CardActions>
            </Card>
        </Container>
    )
}

export default ProductAll;