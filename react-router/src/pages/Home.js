import Header from "../components/Header";

import HomeContent from "../components/content/Home";

function Home () {
    return (
        <>
            <Header />

            <HomeContent />
        </>
    )
}

export default Home;
