import Header from "../components/Header";

import FirstPageContent from "../components/content/FirstPage";

function FirstPage () {
    return (
        <>
            <Header />

            <FirstPageContent />
        </>
    )
}

export default FirstPage;
