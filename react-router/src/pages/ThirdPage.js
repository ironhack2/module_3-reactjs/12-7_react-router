import Header from "../components/Header";

import ThirdPageContent from "../components/content/ThirdPage";

function ThirdPage () {
    return (
        <>
            <Header />

            <ThirdPageContent />
        </>
    )
}

export default ThirdPage;
