import Header from "../components/Header";

import SecondPageContent from "../components/content/SecondPage";

function SecondPage () {
    return (
        <>
            <Header />

            <SecondPageContent />
        </>
    )
}

export default SecondPage;
