import { Routes, Route } from "react-router-dom";
import FirstPage from "./pages/FirstPage";
import Home from "./pages/Home";
import SecondPage from "./pages/SecondPage";
import ThirdPage from "./pages/ThirdPage";


function App() {
  return (
    <div>
      <Routes>
        <Route exact path="/" element={<Home />}></Route>
        <Route path="/firstpage" element={<FirstPage />}></Route>
        <Route path="/secondpage/:param" element={<SecondPage />}></Route>
        <Route path="/thirdpage" element={<ThirdPage />}></Route>
        <Route path="*" element={<ThirdPage />}></Route>
      </Routes>
    </div>
  );
}

export default App;

