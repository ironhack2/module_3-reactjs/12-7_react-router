import { Container, Grid, Typography } from "@mui/material";

function FirstPage() {
    return (
        <Container>
            <Grid container mt={2}>
                <Grid item>
                    <Typography>FirstPage</Typography>
                </Grid>
            </Grid>
        </Container>
    )
}
export default FirstPage;