import {Container, Grid,Typography} from "@mui/material";

function ThirdPage(){
    return(
        <Container>
            <Grid container mt={2}>
                <Grid item>
                    <Typography>ThirdPage</Typography>
                </Grid>
            </Grid>
        </Container>
    )
}
 export default ThirdPage;