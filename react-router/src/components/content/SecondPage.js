import { Container, Grid, Typography } from "@mui/material";
import { useEffect } from "react";

import { useParams, useNavigate } from "react-router-dom";

function SecondPage() {
    const { param } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        if(param === "third") {
            navigate("/thirdpage");
        }
    }, [])

    return(
        <Container>
            <Grid container mt={2}>
                <Grid item>
                    <Typography>Second Page: {param}</Typography>
                </Grid>
            </Grid>
        </Container>
    )
}

export default SecondPage;
