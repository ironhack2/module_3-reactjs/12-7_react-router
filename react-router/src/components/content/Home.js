import {Container, Grid,Typography} from "@mui/material";

function Home(){
    return(
        <Container>
            <Grid container mt={2}>
                <Grid item>
                    <Typography>Home</Typography>
                </Grid>
            </Grid>
        </Container>
    )
}
 export default Home;